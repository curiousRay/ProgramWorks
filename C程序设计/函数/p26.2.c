#include <stdio.h>

int fact(int x){
	if (x == 1)
	{
		return 1;
	}
	else return x*fact(x-1);
}

int main(int argc, char const *argv[])
{
	int n,i;
	int sum = 0;
	scanf("%d",&n);
	for (i = 1; i <= n; i++)
	{
		sum = sum + fact(i);
	}
	printf("%d\n",sum );
	return 0;
}