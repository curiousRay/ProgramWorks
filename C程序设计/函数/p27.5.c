#include <stdio.h>

float H(int n,float x){
	if (n == 0)
	{
		return 1;
	}
	else if (n == 1)
		{
			return 2*x;
		}
		else 
			{
				return 2*x*H(n-1,x)-2*(n-1)*H(n-2,x);
			}
}

int main(int argc, char const *argv[])
{
	int n,i;
	float x;
	scanf("%d%f",&n,&x);
	for (i = 0; i <= n; i++)
	{
		printf("H(%d,%.1f)=%.2f\n",n,x,H(i,x));
	}
	return 0;
}