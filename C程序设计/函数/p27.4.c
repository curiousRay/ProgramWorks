#include <stdio.h>

int gcd(int x,int y){
	if (x%y == 0)
	{
		return y;
	}
	else 
	{
		return gcd(y,x%y);
	}
}

int main(int argc, char const *argv[])
{
	int m,n;
	scanf("%d%d",&m,&n);
	int max = m>=n?m:n;
	int min = m<n?m:n;
	printf("%d\n",gcd(max,min));	
	return 0;
}