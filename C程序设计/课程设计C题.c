#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NUM 20

typedef struct
{
	char ISBN[10];
	char book[30];
	char author[20];
	int edition;
	char press[50];
	int year;
}Bookinfo;

Bookinfo books[NUM]; 

//Bookinfo *books = (Bookinfo*)malloc(NUM * sizeof(Bookinfo));
int num = 0;

int Input(Bookinfo dictList[], int n)//------------------------------INPUT----------------------
{
	int curr;
	for (curr = 0; curr < NUM ; curr++)
	{
		num++;
		printf("Please input ISBN:\n");
		scanf("%s", &dictList[curr].ISBN);
		printf("Please input book's name:\n");
		scanf("%s", &dictList[curr].book);
		printf("Please input author:\n");
		scanf("%s", &dictList[curr].author);
		printf("Please input edition:\n");
		scanf("%d", &dictList[curr].edition);
		printf("Please input press:\n");
		scanf("%s", &dictList[curr].press);
		printf("Please input year:\n");
		scanf("%d", &dictList[curr].year);
		printf("Continue?(y/n):");
		if (getchar() == 'y')
		{
			continue;
		}
		if (getchar() == 'n')
		{
			return num;
		}
	}
}

void Display(Bookinfo dictList[], int n)//------------------------DISPLAY------------------------------
{
	int block, curr;
	printf("书号    书名    作者  版本号  出版社名        出版年\n");
	printf("==============================================================\n");
	for (block = 0; block < n/10+1; block++)
	{
		for (curr = 0; curr < (n>=10?10:n); curr++)
		{
			printf("%-8s%-8s%-6s%-8d%-16s%-16d\n",
			dictList[curr].ISBN, 
			dictList[curr].book, 
			dictList[curr].author, 
			dictList[curr].edition,
			dictList[curr].press,
			dictList[curr].year
			);
		/*	if (10*block+curr+1 == n){}
		*/
		}
		printf("(Press Enter to Page Down)\n");
		if (getchar() == 13)
		{
			system("CLS");
			continue;
		}
	}
}

int Delete (Bookinfo dictList[], int n, char *book)//---------------------DELETE--------------------------
{
	int curr , i;
	for (curr = 0; curr < n; curr++)
	{
		if (strcmp(book, dictList[curr].book) == 0)
		{
			for(i = curr; i < n-1; i++)
			{
				dictList[i] = dictList[i+1];
			}
			/*
			dictList[curr].ISBN = "0";
			dictList[curr].book = "0";
			dictList[curr].author = "0";
			dictList[curr].edition = 0;
			dictList[curr].press = "0";
			dictList[curr].year = 0;
			*/
			return n-1;
		}
	}
	return n;
}

int Delete_a_record(Bookinfo dictList[], int n)
{
	int i = 0, curr, pos, j, books_left;
	char tmp_name[30], tmp_ISBN[10];
	Bookinfo tmp_dictList[NUM];
	printf("Please input the name to delete:");
	scanf("%s", tmp_name);
	for (curr = 0; curr < n; curr++)
	{
		if (strcmp(tmp_name, dictList[curr].book) == 0)
		{
			tmp_dictList[i] = dictList[curr]; 
			i++;
			num = n-1;
		}
		else continue;
	}
	if (i == 0)
	{
		num = n;
		return n;
	}
	books_left = Delete(books, n, tmp_dictList[0].book);//表示第一次删除处理后剩余的个数
	printf("书号    书名    作者  版本号  出版社名        出版年\n");
	printf("==============================================================\n");
	for (pos = 0; pos < i; pos++)
		{
			printf("%-8s%-8s%-6s%-8d%-16s%-16d\n",
			tmp_dictList[pos].ISBN, 
			tmp_dictList[pos].book, 
			tmp_dictList[pos].author, 
			tmp_dictList[pos].edition,
			tmp_dictList[pos].press,
			tmp_dictList[pos].year
			);
		}
	printf("Please input the book number of which you want to delete:\n");
	//在tmp_dictList中查找
	scanf("%s", &tmp_ISBN);
	for (curr = 0; curr < i; curr++)
	{
		if (strcmp(tmp_ISBN, tmp_dictList[curr].ISBN) == 0)
		{
			printf("Are you sure to delete this book?(y/n)");
			getchar();
			if (getchar() == 'y')
			{
				for(j = curr; j < n-1; j++)
				{
					dictList[j] = dictList[j+1];
				}
				printf("delete success!\n");
				num = books_left;
				return num;
			}
			else 
			{
				num =  books_left;
				return num;		
			}
		}
		else continue;
	}
	return num;
}

void Sort_by_name(Bookinfo dictList[], int n)//-------------------------SORT---------------------------
{
	int i, j;
	Bookinfo tmp_dictList;
	for (i = 0; i < n-1; i++)
	{
		for (j = 0; j < n-i-1; j++)
		{
			if (tolower(dictList[j].book[0]) >= tolower(dictList[j+1].book[0]))
				//此处仅考虑了第一个字母排序
			{
				tmp_dictList = dictList[j];
				dictList[j] = dictList[j+1];
				dictList[j+1] = tmp_dictList;
			}
		}
	}
}

int Insert(Bookinfo dictList[], int n, Bookinfo s)//-----------------INSERT-------------------------
{
	int addr, curr;
	//假设已经排好序
	for (curr = 0; curr < n; curr++)//寻找大小关系
	{
		if (tolower(dictList[curr].book[0]) >= tolower(s.book[0]))//将s插在curr之后
		{
			for (addr = n-1; addr >= curr; addr--)//插入时移位
			{
				dictList[addr+1] = dictList[addr];
				break;
			} 
			dictList[curr] = s;
			num = n+1;
			return num;
		}
		else continue;
	}
	return num;
}

int Insert_a_record(Bookinfo dictList[], int n)
{
	Bookinfo s;	
	printf("Please input ISBN:\n");
	scanf("%s", &(s.ISBN));
	printf("Please input book's name:\n");
	scanf("%s", &(s.book));
	printf("Please input author:\n");
	scanf("%s", &(s.author));
	printf("Please input edition:\n");
	scanf("%d", &(s.edition));
	printf("Please input press:\n");
	scanf("%s", &(s.press));
	printf("Please input year:\n");
	scanf("%d", &(s.year));
	printf("Insert success!\n");
	return Insert(books, n, s);
}

int Query(Bookinfo dictList[], int n, Bookinfo book)//------------------------QUERY-------------------------
{
	int i = 0, curr, pos;
	Bookinfo tmp_dictList[NUM];
	for (curr = 0; curr < n; curr++)
	{
		if (strcmp(book.book, dictList[curr].book) == 0)
		{
			tmp_dictList[i] = dictList[curr]; 
			i++;
		}
		else continue;
	}
	if (i > 0)
	{
		printf("书号    书名    作者  版本号  出版社名        出版年\n");
		printf("==============================================================\n");
		for (pos = 0; pos < i; pos++)
		{
			printf("%-8s%-8s%-6s%-8d%-16s%-16d\n",
			tmp_dictList[pos].ISBN, 
			tmp_dictList[pos].book, 
			tmp_dictList[pos].author, 
			tmp_dictList[pos].edition,
			tmp_dictList[pos].press,
			tmp_dictList[pos].year
			);
		}
		return i;
	}
	else return -1;
}		

void Query_a_record(Bookinfo dictList[], int n)
{
	Bookinfo tmp_book;
	printf("Please input the name to query:");
	scanf("%s",tmp_book.book);
	if (Query(books, n, tmp_book) > 0)
	{
		printf("Query success!\n");
	}
	else printf("NOT FOUND!\n");
}

int AddfromText(Bookinfo dictList[], int n, char *filename)//---------------------ADD------------------------------
{
	int num, curr;
	FILE *fp1;
	Bookinfo io_dictList[NUM];
	fp1 = fopen(filename, "r");
	if (!fp1)
	{
		printf("Can't open!\n");
		exit (1);
	}
	fscanf(fp1, "%d", &num);
	for (curr = 0; curr < num; curr++)
	{
		if (feof(fp1) != 0)
		{
			return n+curr;
		}
		fscanf(fp1, "%-8s%-8s%-6s%-8d%-16s%-16d\n",
		&(io_dictList[curr].ISBN), 
		&(io_dictList[curr].book), 
		&(io_dictList[curr].author), 
		&(io_dictList[curr].edition),
		&(io_dictList[curr].press),
		&(io_dictList[curr].year)
		);
		Insert(books, n, io_dictList[curr]);
	}
	fclose(fp1);
	return n+curr;
}

void WritetoText(Bookinfo dictList[], int n, char *filename)//--------------------Write----------------------------
{
	int curr;
	FILE *fp2;
	fp2 = fopen(filename, "w");
	if (!fp2)
	{
		printf("Can't open!\n");
		exit (1);
	}
	fprintf(fp2, "书号    书名    作者  版本号  出版社名        出版年\n");
	fprintf(fp2, "==============================================================\n");
	for (curr = 0; curr < n; curr++)
	{
		fprintf(fp2, "%-8s%-8s%-6s%-8d%-16s%-16d\n",
		dictList[curr].ISBN, 
		dictList[curr].book, 
		dictList[curr].author, 
		dictList[curr].edition,
		dictList[curr].press,
		dictList[curr].year
		);
	}
	fclose(fp2);
}

int Display_main_menu()
{
	char c;
	do
	{
		system("CLS");
		printf("+-----------------------------------------------------------+\n");
		printf("|              图书管理系统      by 雷砺豪(031610118)       |\n");
		printf("+-----------------------------------------------------------+\n");
		printf("1.Input records \n");
		printf("2.Display All Records \n");
		printf("3.Delete a Record \n");
		printf("4.Sort \n");
		printf("5.Insert a Record \n");
		printf("6.Query \n");
		printf("7.Add Records from a Text File \n");
		printf("8.Write to a Text File \n");
		printf("0.Quit \n");
		printf("Input 1-8,0: ");
		c = getchar();                                                                                                          
	}
	while (c<'0' || c>'8');
	return (c-'0');
}

int main(int argc, char const *argv[])//--------------------------------MAIN----------------------------
{
	
	for ( ; ; )
	{
		switch (Display_main_menu())
		{
			case 1:
				printf("The actual number of the books is: %d\n", Input(books, NUM));
				system("PAUSE");
				break;
			case 2:
				Display(books, num);
				system("PAUSE");
				break;
			case 3:
				printf("%d record(s) left\n", Delete_a_record(books, num));
				system("PAUSE");
				break;
			case 4:
				Sort_by_name(books, num);
				system("PAUSE");
				break;
			case 5:
				printf("Now %d record(s)\n", Insert_a_record(books, num));
				system("PAUSE");
				break;
			case 6:
				Query_a_record(books, num);
				system("PAUSE");
				break;
			case 7:
				printf("Now %d record(s)\n", AddfromText(books, num, "Dictory.txt"));
				system("PAUSE");
				break;
			case 8:
				WritetoText(books, num, "Records.txt");
				system("PAUSE");
				break;
			case 0:
				printf("Goodbye! \n");
				system("PAUSE");
				exit(0);
		}
	}
	return 0;
} 