#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
	char str[1000];
	int captial = 0, lowercase = 0, number = 0, space = 0;
	printf("Please input your string:\n");
	gets(str);
	for (int i = 0; i < strlen(str); i++)
	{
		if (str[i]>=65 && str[i]<=90)
		{
			captial++;
		}
		if (str[i]>=97 && str[i]<=122)
		{
			lowercase++;
		}
		if (str[i]>=48 && str[i]<=57)
		{
			number++;
		}
		if (str[i]==32)
		{
			space++;
		}
	}
	int etc = strlen(str) - captial - lowercase - number - space;
	printf("大写字母出现%d次；\n",captial);
	printf("小写字母出现%d次；\n",lowercase);
	printf("数字出现%d次；\n",number);
	printf("空格出现%d次；\n",space);
	printf("其他字符出现%d次。\n",etc);
	return 0;
}
