#include <stdio.h>
#include <string.h>

void my_strcpy(char s1[],char s2[])
{
	int len1 = strlen(s1),len2 = strlen(s2),i,j;
	for (i = 0; i < len1; i++)
	{
		s1[i] = '\0'; 
	}
	for (j = 0; j < len2; j++)
	{
		s1[j] = s2[j]; 
	}
}

int main(int argc, char const *argv[])
{
	int len1 = 0,len2 = 0;
	char s1[100],s2[100];
	printf("Please input s1:\n");
	gets(s1);
  	printf("Please input s2:\n");
  	gets(s2);
	my_strcpy(s1,s2);
	printf("Now s1 is:%s\n", s1);
	printf("Now s2 is:%s\n", s2);
	return 0;
}