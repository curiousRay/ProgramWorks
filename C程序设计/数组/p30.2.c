#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char const *argv[])
{
	int a[10000];
	float time1 = 0;
	float time2 = 0;
	int n,i,j;
	scanf("%d",&n);
	srand((unsigned)time(NULL));
	for (i = 0; i < n; i++)
	{
		a[i] = rand()%10;
	}

	for (j = 0; j < n; j++)
	{
		if (a[j]>=1 && a[j]<=5)
		{
			time1++;
		}
		else time2++;
	}
	printf("在1至5的概率：%.2f%c\n", time1/(time1+time2)*100, 37);
	printf("在6至10的概率：%.2f%c\n", time2/(time1+time2)*100, 37);
	return 0;
}