#include <stdio.h>
#define N 4

void fsum(int a[N][N],int i,int j,int b[2])
{
	int co,li;
	for (co = 0; co < N ;co++)
	{
		b[0] = b[0] + a[i][co];
	}
	for (li = 0; li < N; li++)
	{
		b[1] = b[1] + a[li][j];
	}
}

int main(int argc, char const *argv[])
{
	int a[N][N] = {{3,6,4,6} , {8,3,1,3} , {4,7,1,2} , {2,9,5,3}};
	int b[2] = {0,0};
	int i,j;
	printf("Please input i and j, make sure thay are less than 4:\n");
	scanf("%d%d",&i,&j);
	fsum(a,i,j,b);
	printf("b[0]=%d,b[1]=%d\n",b[0],b[1] );
	return 0;
}