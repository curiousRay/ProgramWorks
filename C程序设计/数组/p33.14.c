#include <stdio.h>
#include <string.h>

void reverse(char s[])
{
	int tmp;
	for (unsigned int i = 0; i < strlen(s)/2; i++)
	{
		tmp = s[i];
		s[i] = s[strlen(s)-i-1];
		s[strlen(s)-i-1] = tmp;
	}
}

int main(int argc, char const *argv[])
{
	char str[100];
	printf("Please input the string to reverse:\n");
	gets(str);
	reverse(str);
	printf("The reversed array is:%s\n",str );
	return 0;
}
