#include <stdio.h>

void output(int a[],int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		printf("%d\t",a[i]);
	}
	printf("\n");
}

int deleteElement(int a[],int n,int x)
{
	int cnt = 0,i,j;
	for (i = 0; i < n; i++)
	{
		if (a[i] == x)
		cnt++;
	}
	for (i = 0; i < n; i++)
		{
			if (a[i] == x)
			{
				for (j = i; j < n-1 ; j++)
				{
					a[j]=a[j+1];				
				}
			}
		}	
	return cnt;
}

int main(int argc, char const *argv[])
{
	int a[10000],x,i,num = 0;
	printf("Input your array:\n");
	for (i = 0; i < 10000; i++)
	{
		scanf("%d",&a[i]);
		num++;
		if (getchar() == '\n')
		break;
	}
	printf("Your array is:\n");
	output(a,num);
	printf("Please input the element to delete:\n");
	scanf("%d",&x);
	int cnt = deleteElement(a,num,x);
	printf("Processed array is:\n");
	output(a,num-cnt);
	return 0;
}
