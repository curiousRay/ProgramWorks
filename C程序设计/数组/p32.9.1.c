#include <stdio.h>
#define N 4
#define M 5

int main(int argc, char const *argv[])
{
	int a[N][M] = {{3,6,4,6,1} , {8,3,1,3,2} , {4,7,1,2,7} , {2,9,5,3,3}};
	int sum = 0,i,j;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < M; j++)
		{
			printf("%d\t",a[i][j]);
			if (i == 0 || i == N-1 || j == 0 || j == M-1)
			{
				sum = sum + a[i][j];
			}
			if (j == M-1)
			{
				printf("\n");
			}
		}
	}
	printf("Sum is:%d\n",sum );
	return 0;
}