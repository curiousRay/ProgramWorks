#include <stdio.h>
#include <string.h>

struct book
		{
			char bookID[20];
			char name[20];
			double price;
		};

void input(struct book bs[], int n)
{
	int i;
	for (i = 0; i < n; i++)
	{
		printf("Please input the price of book %s:\n",bs[i].name );
		scanf("%lf",&bs[i].price);	
	}
}

double average(struct book bs[], int n)
{
	double sum = 0;
	int i;
	for (i = 0; i < n; i++)
		{
			sum = sum + bs[i].price;
		}	
	return sum/n;
}

int findMax(struct book bs[], int n)
{	
	int max_subscpt = bs[0].price, i;
	for (i = 0; i < n; i++)
	{
		if (bs[i].price >= bs[max_subscpt].price)
		{
			max_subscpt = i;
		}
	}
	return max_subscpt;
}

void print(struct book bs[], int n)
{
	printf("\n书号\t书名\t价格\n");
	printf("-- -- -- -- -- -- -- -- -\n");
	for (int i = 0; i < n; i++) {
		printf("%s\t%s\t%.2lf\n", bs[i].bookID, bs[i].name, bs[i].price );
	}
}

void sort(struct book bs[], int n)
{
	struct book tmp;
	int i,j;
	for(j = 0; j < n; j++)   
	{
		for(i = 0; i < n-j-1; i++)
		{
			if(bs[i].price > bs[i+1].price)
			{
				tmp = bs[i];
				bs[i] = bs[i+1];
				bs[i+1] = tmp;
			}
		}
	}
}

int main(int argc, char const *argv[])
{
	struct book books[4] = {{"0101","Computer",1.5},{"0102","Programming",4.1},{"0103","Math",3.3},{"0104","English",1.2}};	
	input(books, 4);
	print(books, 4);
	average(books, 4);
	int max_subscpt = findMax(books, 4);
	printf("\nThe subscript of the dearest book is :%d\n\n", max_subscpt);
	printf("书号\t书名\t价格\n");
	printf("-- -- -- -- -- -- -- -- -\n");
	printf("%s\t%s\t%.2f\n", books[max_subscpt].bookID, books[max_subscpt].name, books[max_subscpt].price );
	printf("\nSorted:");
	sort(books, 4);
	print(books, 4);
	return 0;
}