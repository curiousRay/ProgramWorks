#include <stdio.h>
#include <string.h>

void getDigits(char* s1, char* s2)
{
	for (unsigned int i = 0, j = 0; i < strlen(s1); i++)
	{
		if (s1[i] >= 48 && s1[i] <=57)
		{
			s2[j] = s1[i];
			j++;
		}
	}
	s2[j] = '\0';
}

int main(int argc, char const *argv[])
{
	char s1[100]; 
	char s2[100]; 	
	gets(s1);
	getDigits(s1, s2);
	puts(s2);
	return 0;
}