#include <stdio.h>
#include <ctype.h>
#include <string.h>

int palin(char *s1)
{
	char *head = s1, *tail = head+strlen(s1)-1;
	do{
		if(isalpha(*head) == 0)
		{
			do
			{
				head++;
			}
			while(isalpha(*head) != 0);
		}
		if(isalpha(*tail) == 0)
		{
			do{
				tail--;
				printf("%c\n",*head);
			}
			while(isalpha(*tail) != 0);
		}
		if(toupper(*head) != toupper(*tail))
		{
			return 0;
		}
		else
		{
		 	head++;
		 	tail--; 
		}
	}
	while(head>=tail);	
}

int main(int argc, char const *argv[])
{
	char s1[100];
	gets(s1);
	char temp[100];
	strcpy(temp,s1);
	if (palin(s1) == 0)
	{
		printf("no\n");
	}
	else printf("yes\n");
	return 0;
}