#include <stdio.h>

void statistic(int* a,int n,int* posinum_ptr,int* neganum_ptr)
{
	int i;
	for (i = 0; i < n-1; i++)
	{
		if (*a>0)
		{
			(*posinum_ptr)++;
			a++;
		}
		if (*a<0)
		{
			(*neganum_ptr)++;
			a++;
		}
	}
}

int main(int argc, char const *argv[])
{
	int i, a[10], *p = a, posi_num = 0, nega_num = 0;
	int* posinum_ptr = &posi_num;
	int* neganum_ptr = &nega_num;
	printf("Please input numbers:\n");	
	for (i = 0; i < 10; i++)
	{
		scanf("%d",p+i);
	}
	statistic(p,10,posinum_ptr,neganum_ptr);
	printf("正数有%d个，负数有%d个\n",posi_num, nega_num );
	return 0;
}