#include <stdio.h>
#include <ctype.h>
#include <string.h>

void filter(char *s1)
{
	char *p = s1, s2[100];
	int j = 0;
	for (unsigned int i = 0; i < strlen(p); i++)
	{
		if (isalpha(p[i]) != 0)
		{
			s2[j] = s1[i];
			j++;
		}
	}
	s2[j] = '\0';
	strcpy(s1,s2);	
	for (unsigned int k = 0; k < strlen(s1); k++)
	{
		if (s1[k] >= 97 && s1[k] <= 122)
		{
			s1[k] = s1[k] - 32;
		}
	}
}

int palin(char *s1)
{
	char *head = s1, *tail = s1+strlen(s1)-1;
	while(head<tail)
	{
		if (*head == *tail)
		{
			head++;
			tail--;
		}
		else return 0; 
	} 
}

int main(int argc, char const *argv[])
{
	char s1[100];
	gets(s1);
	char temp[100];
	strcpy(temp,s1);
	filter(s1);
	if (palin(s1) == 0)
	{
		printf("no\n");
	}
	else printf("yes\n");
	return 0;
}