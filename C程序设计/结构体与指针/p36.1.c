#include <stdio.h>
void swap(int *px, int *py)
{
	int t;
	t = *px;
	*px = *py;
	*py = t;
}
int main(int argc, char const *argv[])
{
	int a, b, c;
	scanf("%d%d%d", &a, &b, &c);
	if (a>b)
	{
		swap(&a, &b);
	}
	if (a>c)
	{
		swap(&a, &c);
	}
	if (b>c)
	{
		swap(&b, &c);
	}
	printf("a is:%d\tb is:%d\tc is:%d\n",a,b,c );
	return 0;
}