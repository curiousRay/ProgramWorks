#include <stdio.h>
#include <stdlib.h> 

int compute(int num)
{
	if (num < 10)
	{
		return num * 25;
	}
	if (num >= 10 && num < 50)
	{
		return num * 24;
	}
	if (num >= 50 && num < 100)
	{
		return num *22;
	}
	if (num >= 100)
	{
		return num * 20;
	}
	else exit(1);
}

int main(int argc, char const *argv[])
{
	FILE* fin, *fout;
	fin = fopen("nums.txt", "r");
	fout = fopen("sales.txt", "w");
	int a[100], num = 0, sales = 0;
	if (!fin)
	{
		printf("Could not open!\n");
		exit(1);
	}
	fprintf(fout, "  num\t\tsales\n-- -- -- -- -- -- -- -- \n");
	for (int i = 0; i < 100; i++)
	{
		fscanf(fin, "%d", &a[i]);
		fprintf(fout, "  %d\t\t%d\n", a[i], compute(a[i]));
		num = num + a[i];
		sales = sales + compute(a[i]);
		if (feof(fin))
		{
			break;
		}	
	}
	fprintf(fout, "-- -- -- -- -- -- -- --\n");
	fprintf(fout, "total num = %d\ntotal sales = %d\n", num, sales);
	fclose(fin);
	fclose(fout);
	return 0;
}