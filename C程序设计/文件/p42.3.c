#include <stdio.h>
#include <stdlib.h> 

void sort(float arr[], int len)
{
	int i, j;
	float temp;
	for (i = 0; i < len - 1; i++)
	{
		for (j = 0; j < len - 1 - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

int main(int argc, char const *argv[])
{
	FILE* fin, *fout;
	float a[100];
	int count = 0;
	fin = fopen("input.txt", "r");
	fout = fopen("output.txt", "w");
	if (!fin)
	{
		printf("Could not open!\n");
		exit(1);
	}
	for (int i = 0; i < 100; i++)
	{
		fscanf(fin, "%f", &a[i]);
		count++;
		if (feof(fin))
		{
			break;
		}	
	}
	sort(a, count);
	for (i = 0; i < count; i++)
	{
		fprintf(fout, "%.2f\n", a[i]);	
	}
	fclose(fin);
	fclose(fout);
	return 0;
}