#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	FILE* fin1, *fin2, *fout;
	int a[100], b[100], count1 = 0, count2 = 0;
	int var1 = 0, var2 = 0;
	fin1 = fopen("w1.txt", "r");
	fin2 = fopen("w2.txt", "r");
	fout = fopen("w3.txt", "w");
	if (!fin1 || !fin2)
	{
		printf("Could not open!\n");
		exit(1);
	}
	for (int i = 0; i < 100; i++)
	{
		fscanf(fin1, "%d", &a[i]);
		count1++;
		if (feof(fin1))
		{
			break;
		}	
	}
	for (int j = 0; j < 100; j++)
	{
		fscanf(fin2, "%d", &b[j]);
		count2++;
		if (feof(fin2))
		{
			break;
		}	
	}
	for (int k = 0; k<100; k++ )
	{
		if (var1 == count1)
		{
			for (int rest2 = var1; rest2 < count2; rest2++)
			{
				fprintf(fout, "%d\n", b[rest2]);
			}
			break;
		}
		if (var2 == count2)
		{
			for (int rest1 = var2; rest1 < count1; rest1++)
			{
				fprintf(fout, "%d\n", a[rest1]);
			}
			break;
		}

		if (a[var1] < b[var2])
		{
			fprintf(fout, "%d\n", a[var1]);
			var1++;
			continue;
		}
		if (a[var1] > b[var2])
		{
			fprintf(fout, "%d\n", b[var2]);
			var2++;
			continue;
		}
		if (a[var1] == b[var2])
		{
			fprintf(fout, "%d\n", a[var1]);
			fprintf(fout, "%d\n", b[var2]);
			var1++;
			var2++;
			continue;
		}
	}
	fclose(fin1);
	fclose(fin2);
	fclose(fout);
	return 0;
}