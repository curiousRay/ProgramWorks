clc;    %清除命令窗口内容
clear;
gg = []; wwn = []; zz = []; %初始化空数组；
%for i = 1 : length(k) %根据开环增益的个数，设置循环次数
        num = 1; %输入分子多项式； den = conv([0.1 1 0],
        den = [0.05 1 0]; %输入分母多项式；
%         num = [ 30]; %输入分子多项式；
%         den = conv([0.1 1 0], [0.2 1]); %输入分母多项式；
        g0 = tf(num,den); %系统的开环传递函数；
        g = feedback(g0,1); %系统的闭环传递函数；
        %[wn,z,p] = damp(g);%系统的自然频率、阻尼比和闭环极点；
        gg = [gg,g]; %存储对应不同开环增益下的系统的闭环传递函数；
        %wwn = [wwn,wn]; %存储对应不同开环增益下的系统的自然频率；
        %zz = [zz,z]; %存储对应不同开环增益下的系统的阻尼比；
    
%[y,time] = step(gg(i)); %此time为用来采样的时间
%disp(['max is ', num2str(max(y))]); %显示峰值

%[ymax,tp]=max(y);
%peak_time = spline(y, time, ymax);
%disp(['peak_time is ', num2str(peak_time)]);
%final_value = y(104); %终值
%while (abs(y-final_value) <= final_value*0.05)
%    disp(time) %调节时间
%end

%end
step(gg(1),'-');
% margin(g0);
% [Gm,Pm,Wcg,Wcp] = margin(g0);
%在同一坐标下使用不同线型绘制不同开环增益下的系统阶跃响应曲线。