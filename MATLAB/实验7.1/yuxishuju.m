clc;
clear;
%w = 2*pi*f
A = [];
w = [];
phi = [];
L = [];
n = 22.361;
t = 0.2236;
f = [1 2 2.5 3 3.2 3.3 3.4 3.5 4 5.3 8 12];
for i = 1:length(f)
w(i) = f(i)*2*pi;

A(i) = 1/(sqrt((1-w(i)^2/n^2)^2+4*t^2*w(i)^2/n^2));

if (w(i) < n || w(i) == n)
    phi(i) =  -atan(2*t*w(i)/n/(1 - w(i)^2/n^2));
else
    phi(i) = atan(2*t*w(i)/n/(w(i)^2/n^2 - 1)) - pi;
end

phi(i) = radtodeg(phi(i)); %换成角度

L(i) = 20*log10(A(i));

end

%实验值
A1 = [];
L1 = [];
dt = [];
phi1 = [];

xpp = [-22.2 -25.9 -31.5 -49.2 -58.0 -59.9 -62.1 -70.2 -85.0 -75.8 -53.7 -39.6];
ypp = [2177 2770 3404 4355 4588 4630 4672 4503 3573 1281 482 222];
for j = 1 : length(xpp)
    A1(j) = ypp(j) / 2017;
    L1(j) = 20*log10(A1(j));
    phi1(j) = xpp(j) * f(j) * 360 / 1000;
end

    