clc;
clear;
k=100;
gg = []; wwn = []; zz = []; %初始化空数组；
num = 50; %输入分子多项式；
den = [0.1 1 0]; %输入分母多项式；
g0 = tf(num,den); %系统的开环传递函数；
g = feedback(g0,1); %系统的闭环传递函数；
[wn,z,p] = damp(g);%系统的自然频率、阻尼比和闭环极点；
gg = [gg,g]; %存储对应不同开环增益下的系统的闭环传递函数；
wwn = [wwn,wn]; %存储对应不同开环增益下的系统的自然频率；
zz = [zz,z]; %存储对应不同开环增益下的系统的阻尼比；
figure
step(gg(1),'-'); %阶跃响应曲线
figure
%[Gm, Pm, Wg, Wp] = margin(g);
BW = bandwidth(g)%wb
[Mp, fp] = getPeakGain(g)%fp => wr
 