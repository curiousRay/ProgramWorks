clc;
clear;
img = imread('images\kids.tif');

img = double(img) / 256;
figure,subplot(422),imshow(img),title('原图kids.tif');
r = 0:0.001:1;

s1 = r .^ 0.6;
s2 = r .^ 0.4;
s3 = r .^ 0.3;
a1 = imadjust(img, [], [], 0.6);
a2 = imadjust(img, [], [], 0.4);
a3 = imadjust(img, [], [], 0.3);
subplot(423), imshow(a1), title('s^0.6处理的kids.tif');
subplot(424), plot(r, s1), title('s^0.6变换函数曲线');
subplot(425), imshow(a2), title('s^0.4处理的kids.tif');
subplot(426), plot(r, s2), title('s^0.4变换函数曲线');
subplot(427), imshow(a3), title('s^0.3处理的kids.tif');
subplot(428), plot(r, s3), title('s^0.3变换函数曲线');
imwrite(a1, 's^0.6_kids.tif');
imwrite(a2, 's^0.4_kids.tif');
imwrite(a2, 's^0.3_kids.tif');