clc;
clear;
img = imread('images\circuit.tif');

img = double(img) / 256;
figure,subplot(222),imshow(img),title('原图circuit.tif');
r = 0:0.001:1;
s = 1 - r;
img = 1 - img;
subplot(223), plot(r, s), title('反变换函数');
subplot(224), imshow(img), title('反变换circuit.tif');
imwrite(img, 'Negative_circuit.tif');