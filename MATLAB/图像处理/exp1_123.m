clc;
clear;
img = imread('images\rice.png');

img = double(img) / 256;
figure,subplot(322),imshow(img),title('原图rice.png');
r = 0:0.001:1;

%t1, t2用于绘制折线
t1 = (r < 0.35) .* r * 0.3;
t1 = t1 + (r <= 0.65) .* (r >= 0.35) .* (0.105 + 2.6333 * (r - 0.35));
t1 = t1 + (r > 0.65) .* (1 + 0.3 * (r - 1));
t2 = (r <= 0.5) .* (r .^ 5) * 15.9744;
t2 = t2 + (r > 0.5) .* ((r - 0.5) .^ 0.2 + 0.12);
%a1, a2用于变换图像
a1 = (img < 0.35) .* img * 0.3;
a1 = a1 + (img <= 0.65) .* (img >= 0.35) .* (0.105 + 2.6333 * (img - 0.35));
a1 = a1 + (img > 0.65) .* (1 + 0.3 * (img - 1));
a2 = (img <= 0.5) .* (img .^ 5) * 15.9744;
a2 = a2 + (img > 0.5) .* ((img - 0.5) .^ 0.2 + 0.12);

subplot(323),plot(r, t1),title('变换函数T1');
subplot(324),imshow(a1),title('T1变换后rice.png');
subplot(325),plot(r, t2),title('变换函数T2');
subplot(326),imshow(a2),title('T2变换后rice.png');
imwrite(a1, 'T1_rice.png');
imwrite(a2, 'T2_rice.png');