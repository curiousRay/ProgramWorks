clc;
clear;
img = imread('images\mountain.jpg');

img = uint8(img);
r = 0:1:255;
n = (r <= 5) .* r * 1400;
n = n + (r > 5) .* (r <= 20) .* (7000 - r * 310);
n = n + (r > 20) .* (r <= 180) .* (900 - r * 5);
n = n + (r > 180) .* (r <= 225) .* (-1440 + 8 * r);
n = n + (r > 225) .* (r <= 255) .* (3060 - 12 * r);
[img_avg, transfun] = histeq(img, n);

figure,subplot(231),imshow(img),title('原图mountain.jpg');
subplot(232),imhist(img),title('原直方图');
subplot(234),imshow(img_avg),title('匹配运算');
subplot(235),imhist(img_avg),title('匹配运算直方图');
subplot(236),plot(transfun),title('变换函数');
imwrite(img_avg, 'mount.jpg')

% 直方图均衡化算法
img2 = im2double(img);
[m, n] = size(img2);

