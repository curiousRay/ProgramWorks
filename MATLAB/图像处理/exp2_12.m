clc;
clear;
img = imread('images\mountain.jpg');

[img_avg, transfun] = histeq(img, 256);
figure,subplot(231),imshow(img),title('原图mountain.jpg');
subplot(232),imhist(img),title('原直方图');
subplot(234),imshow(img_avg),title('均衡化AVGmountain.jpg');
subplot(235),imhist(img_avg),title('均衡化直方图');
subplot(236),plot(transfun),title('变换函数');
imwrite(img_avg, 'mount.jpg')