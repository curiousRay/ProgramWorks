clc;
clear;
img = imread('images\Picture.jpg');

img = img(:, :, 1);
img = im2double(img);
figure, imhist(img), title('原直方图');
r = (0: 0.001: 1);
s = (r < 0.3) .* r * 1.1 + (r > 0.65) .* r * 0.8
s = s + (r >= 0.3) .* (r <= 0.65) .* (0.11 + r * 1.5);

img = (img < 0.3) .* img * 1.1 + (img > 0.65) .* img * 0.8
img = img + (img >= 0.3) .* (img <= 0.65) .* (0.11 + img * 1.5);

figure, imhist(img), title('变换后直方图');
figure, imshow(img), title('变换后图');
figure, plot(r, s), title('变换函数');axis on;
imwrite(img, '改后Picture.jpg')