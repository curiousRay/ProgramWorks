clear;
clc;

x=[83 78 73 68 63 58 53];
y=[1378 1395 1420 1460 1504 1544 1589];
plot(x,y,'k.','MarkerSize', 10);
title('直流电机弱磁调速 U_a=198V');
grid;
xlabel('励磁电流I_f/mA','FontWeight','bold','FontSize',12);
ylabel('转速n/rpm','FontWeight','bold','FontSize',12);
