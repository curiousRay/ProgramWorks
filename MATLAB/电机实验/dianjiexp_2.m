clear;
clc;

U1 = [110 143 176 209 220];  %修改此处数据
U2 = [27.6 35.8 44.1 52.3 55]; %修改此处数据
plot(U1, U2, 'k.', 'MarkerSize', 10);
title('原边电压U_1和副边电压U_2的关系');
grid;
xlabel('原边电压U_1/V', 'FontWeight', 'bold', 'FontSize', 12);
ylabel('副边电压U_2/V', 'FontWeight', 'bold', 'FontSize', 12);

%figure

Ik = [140 194 248 302 356 410];    %修改此处数据
Uk = [10.1 14.2 18.1 22 26.2 30.3];   %修改此处数据
Pk = [0.55 1.1 1.82 2.75 3.9 5.24]; %修改此处数据
title('短路实验');
grid;
hold on;
[AX,H1,H2] = plotyy(Ik, Uk, Ik, Pk, 'plot');
legend([H1, H2], '电压', '功率');
set(H1, 'LineStyle', 'none', 'Marker', '.' , 'color', 'k','MarkerSize', 10);
set(H2, 'LineStyle', 'none', 'Marker', '+', 'color', 'k');
xlabel('短路电流I_k/mA', 'FontWeight', 'bold', 'FontSize', 12);
ylabel1 = get(AX(1), 'Ylabel');
ylabel2 = get(AX(2), 'Ylabel');
set(ylabel1, 'String', '电压U_k/V', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
set(ylabel2, 'String', '功率P_k/W', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
set(AX(1), 'XColor', 'k', 'YColor', 'k');
set(AX(2), 'XColor', 'k', 'YColor', 'k');

%figure

U0 = [27.5 33 38.5 44 49.5 55 60.5 66];    %修改此处数据
I0 = [43 49 56 65 76 91 111 138];   %修改此处数据
P0 = [0.6 0.83 1.08 1.38 1.73 2.14 2.66 3.29]; %修改此处数据
title('空载实验');
grid;
hold on;
[AX,H1,H2] = plotyy(U0, I0, U0, P0, 'plot');
legend([H1, H2], '电流', '功率');
set(H1, 'LineStyle', 'none', 'Marker', '.' , 'color', 'k','MarkerSize', 10);
set(H2, 'LineStyle', 'none', 'Marker', '+', 'color', 'k');
xlabel('空载电压U_0/V', 'FontWeight', 'bold', 'FontSize', 12);
ylabel1 = get(AX(1), 'Ylabel');
ylabel2 = get(AX(2), 'Ylabel');
set(ylabel1, 'String', '电流I_0/mA', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
set(ylabel2, 'String', '功率P_0/W', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
set(AX(1), 'XColor', 'k', 'YColor', 'k');
set(AX(2), 'XColor', 'k', 'YColor', 'k');

%figure

I1 = [0 100 140 210 280 350 410];    %修改此处数据
I2 = [0.09 0.453 0.594 0.888 1.154 1.433 1.698];   %修改此处数据
U1 = [219 216 215 212 208.3 205.5 202.5]; %修改此处数据
title('负载实验     cos\phi=1  U_2=U_N_2=55V');
grid;
hold on;
[AX,H1,H2] = plotyy(I1, I2, I1, U1, 'plot');
legend([H1, H2], '输出电流', '输出电压');
set(H1, 'LineStyle', 'none', 'Marker', '.' , 'color', 'k','MarkerSize', 10);
set(H2, 'LineStyle', 'none', 'Marker', '+', 'color', 'k');
xlabel('负载电流I_1/mA', 'FontWeight', 'bold', 'FontSize', 12);
ylabel1 = get(AX(1), 'Ylabel');
ylabel2 = get(AX(2), 'Ylabel');
set(ylabel1, 'String', '输出电流I_2/A', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
set(ylabel2, 'String', '输出电压U_1/V', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
set(AX(1), 'XColor', 'k', 'YColor', 'k');
set(AX(2), 'XColor', 'k', 'YColor', 'k');

figure

I1 = [0 149 200 250 280 330 410];    %修改此处数据
I2 = [0.094 0.657 0.888 1.064 1.180 1.418 1.734];   %修改此处数据
%U1 = [218.3 207.5 206 199.5 200.6 197.4 190.6]; %修改此处数据
U1 = [218.3 209.5 206 202.5 200.6 197.4 190.6];
title('接纯电阻负载实验     cos\phi=0.8  U_2=U_N_2=55V');
grid;
hold on;
[AX,H1,H2] = plotyy(I1, I2, I1, U1, 'plot');
legend([H1, H2], '输出电流', '输出电压');
set(H1, 'LineStyle', 'none', 'Marker', '.' , 'color', 'k','MarkerSize', 10);
set(H2, 'LineStyle', 'none', 'Marker', '+', 'color', 'k');
xlabel('负载电流I_1/mA', 'FontWeight', 'bold', 'FontSize', 12);
ylabel1 = get(AX(1), 'Ylabel');
ylabel2 = get(AX(2), 'Ylabel');
set(ylabel1, 'String', '输出电流I_2/A', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
set(ylabel2, 'String', '输出电压U_1/V', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
set(AX(1), 'XColor', 'k', 'YColor', 'k');
set(AX(2), 'XColor', 'k', 'YColor', 'k');
hold off;