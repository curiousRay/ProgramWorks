clear;
clc;

% p1 = [221.1 183.2 139.1 98 59.7 37.03];  %修改此处数据
% p2 = [140.5 115.2 88.47 59.98 30.52 0];  %修改此处数据
% t2 = [1 0.8 0.6 0.4 0.2 0];
% n = [1342 1375 1408 1432 1457 1472];
% plot(p2, n, 'k.', 'MarkerSize', 10);
% title('异步电动机工作特性 n=f(P_2)'); %修改此处数据
% grid;
% xlabel('输出功率P_2/V', 'FontWeight', 'bold', 'FontSize', 12);
% ylabel('转速/rpm', 'FontWeight', 'bold', 'FontSize', 12);

p1 = [221.1 183.2 139.1 98 59.7 37.03];  %修改此处数据
p2 = [140.5 115.2 88.47 59.98 30.52 0];  %修改此处数据
i1 = [0.764 0.657 0.561 0.493 0.443 0.424]; %修改此处数据

u0 = [261.4 231.3 206.2 181.4 156.1 131.1 105.4 80.2];    %修改此处数据
i0 = [0.551 0.454 0.402 0.345 0.3 0.271 0.258 0.246];   %修改此处数据
p0 = [46.96 41.63 38.41 35.08 33.57 31.74 30.96 30.43]; %修改此处数据
p0cu1 = [];
p0_1 = [];
u0_2 = [];
uk = [60.1 49.4 37.4 25.7 14];    %修改此处数据
ik = [0.659 0.536 0.405 0.281 0.153];   %修改此处数据
pk = [37.95 25.76 14.82 7.12 1.94]; %修改此处数据
cosphi1 = []; %修改此处数据
zeta = []; %修改此处数据

for i=1:length(p1)
    zeta(i) = p2(i)/p1(i);
    p0cu1(i) = 37.4 * i0(i) * i0(i);
    p0_1(i) = p0(i) - p0cu1(i);
    u0_2(i) = u0(i) * u0(i);
end
plot(p2, zeta, 'k.', 'MarkerSize', 10);
title('异步电动机工作特性 \eta=f(P_2)'); %修改此处数据
grid;
xlabel('输出功率P_2/W', 'FontWeight', 'bold', 'FontSize', 12);
ylabel('效率\eta', 'FontWeight', 'bold', 'FontSize', 12);

figure
% 
% i_f = [83 78 73 68 63 58 53]; %修改此处数据
% n = [1378 1395 1420 1460 1504 1544 1589]; %修改此处数据
% plot(i_f, n, 'k.', 'MarkerSize', 10);
% title('直流电机弱磁调速 U_a=198V'); %修改此处数据
% grid;
% xlabel('励磁电流I_f/mA', 'FontWeight', 'bold', 'FontSize',12);
% ylabel('转速n/rpm', 'FontWeight', 'bold', 'FontSize', 12);
% 
% figure

u0 = [261.4 231.3 206.2 181.4 156.1 131.1 105.4 80.2];    %修改此处数据
i0 = [0.551 0.454 0.402 0.345 0.3 0.271 0.258 0.246];   %修改此处数据
p0 = [46.96 41.63 38.41 35.08 33.57 31.74 30.96 30.43]; %修改此处数据
uk = [60.1 49.4 37.4 25.7 14];    %修改此处数据
ik = [0.659 0.536 0.405 0.281 0.153];   %修改此处数据
pk = [37.95 25.76 14.82 7.12 1.94]; %修改此处数据
Rr = [0 2 5 15];

n1 = [1468 1459 1445 1419 1361];
n2 = [1451 1437 1408 1346 1088];
u = [220 187 154 121 88];
title({'异步电动机调压调速 R_r=0\Omega'});  %修改此处数据
grid;
hold on;
[AX,H1,H2] = plotyy(u, n1, u, n2, 'plot');
legend([H1, H2], 'T_2=0Nm', 'T_2=0.2Nm');
set(H1, 'LineStyle', 'none', 'Marker', '.' , 'color', 'k','MarkerSize', 10);
set(H2, 'LineStyle', 'none', 'Marker', '+', 'color', 'k');
xlabel('线电压U/V', 'FontWeight', 'bold', 'FontSize', 12);
ylabel1 = get(AX(1), 'Ylabel');
%ylabel2 = get(AX(2), 'Ylabel');
set(ylabel1, 'String', '转速n/rpm', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
%set(ylabel2, 'String', '短路功率P_k/W', 'color', 'k', 'FontWeight', 'bold', 'FontSize', 12);
set(AX(1), 'XColor', 'k', 'YColor', 'k');
set(AX(2), 'XColor', 'k', 'YColor', 'k');
%set(AX(1), 'yTick', t2(9):0.12:t2(1) + 0.2);
%set(AX(2), 'yTick', n(1) - 100:50:n(9));
hold off;