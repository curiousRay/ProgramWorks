
A = [0 2
     -1 -1];
B = [0
     1];
C = [1 0];

P = ctrb(A, B);
if rank(A) == rank(P)
    disp('系统稳定')
else
    disp('系统不稳定')
end

poles = [-3+3*1i -3-3*1i];%主导极点
K = acker(A, B, poles)

obpoles = [-12 -12];%观测器极点
Ht = acker(A', C', obpoles);
H = Ht'

