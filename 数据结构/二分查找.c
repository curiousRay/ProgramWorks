#include <stdio.h>
#define ElemType int
#define MAX 100

int binary_search (int array[], int key, int length) 
{
  int left = 0;
  int right = length - 1;
  while (left <= right) 
  {
    int mid = (left + right) / 2;
    if (array[mid] == key) 
    {
      return mid + 1;
    }
      else if (array[mid] < key) 
      {
        left = mid + 1;
      }
      else 
      {
        right = mid - 1;
      }
  }
  return 0;
}


int main (void)
{
  ElemType x, seq[MAX];
  int i = 0, j, value, flag;
  printf("input your origin sequence, type -1 to stop:\n");
  scanf("%d", &x);
  while (x != -1)
  {
    seq[i] = x;
    i++;
    scanf("%d", &x);
  }
  printf("your sequence is:\n");
  for (j = 0; j < i; j++)
  {
    printf("%d\t", seq[j]);
  }
  printf("\ninput the elem you want to lookup\n");
  scanf("%d", &value);
  flag = binary_search (seq, value, i);
  if (flag == 0)
  {
    printf("not found!\n");
  }
  else printf("elem found at position %d\n", flag);
  return 0;
}