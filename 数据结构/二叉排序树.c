#include <stdio.h>
#include <malloc.h>
#define BiTNode struct bitnode
BiTNode
{
  int data;
  BiTNode *LChild, *RChild;
};

BiTNode *init (BiTNode *bt)
{
  bt = NULL;
  return bt;
}

int is_data_exist (BiTNode *root, int num, BiTNode **lastnode)
{
  BiTNode *tmp = root;
  if (!tmp) return -1; // the tree is empty
  while (1)
  {
    if (tmp -> data == num) return 0; // the requested num exists
    if (tmp -> data < num)
    {
      if (tmp -> RChild == NULL)
      {
        (*lastnode) = tmp;
        return 1; // the requested num does not exist
      }
      tmp = tmp -> RChild;
    }
    else
    {
      if (tmp -> LChild == NULL)
      {
        (*lastnode) = tmp;
        return 1;
      }
      tmp = tmp -> LChild;
    }
  }
}

BiTNode *create (BiTNode *root, int num)
{
  int result = 0;
  BiTNode *pLastNode = NULL;

  result = is_data_exist (root, num, &pLastNode);
  if (result == 0) return root;
  if (result == 1)
  {
    BiTNode *p;
    p = (BiTNode*)malloc(sizeof(BiTNode));
    p -> data = num;
    p -> LChild = NULL;
    p -> RChild = NULL;
    if (pLastNode -> data < num)
    {
      pLastNode -> RChild = p;
    }
    else
    {
      if(pLastNode -> data > num)
      {
        pLastNode -> LChild = p;
      }  
    }
  }
  if (result == -1)
  {
    BiTNode *p = (BiTNode*)malloc(sizeof(BiTNode));
    p -> data = num;
    p -> LChild = NULL;
    p -> RChild = NULL;
    root = p;
  }
  
  return root;
}

void in_order (BiTNode *bt)
{
  if (bt == NULL) return ;
  in_order (bt -> LChild);
  printf("%d\t", bt -> data);
  in_order (bt -> RChild);
}

int main (void)
{
  int input;
  BiTNode *bt;
  bt = init (bt);

  printf("input numbers, type -1 to stop:\n");
  scanf("%d", &input);
  while (input != -1)
  {
    bt = create (bt, input);
    scanf("%d", &input);
  }
  printf("in order sequence:\n");
  in_order (bt);
  printf("\n");
  
  return 0;
}