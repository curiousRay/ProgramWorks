#include <stdio.h>
#define ElemType int
#define MAX 100

void bubble_sort (ElemType seq[], ElemType size)
{
  int i, j, flag = 0, tmp;
  for (i = 0; (i < size && flag == 0); i++)
  {
    flag = 1;
    for (j = 0; j < size - i - 1; j++)
    {
      if (seq[j + 1] < seq[j])
      {
        flag = 0;
        tmp = seq[j];
        seq[j] = seq[j + 1];
        seq[j + 1] = tmp;
      }
    }
  }
}

int main (void)
{
  ElemType x, seq[MAX];
  int i = 0, j;
  printf("input your origin sequence, type -1 to stop:\n");
  scanf("%d", &x);
  while (x != -1)
  {
    seq[i] = x;
    i++;
    scanf("%d", &x);
  }
  printf("your sequence is:\n");
  for (j = 0; j < i; j++)
  {
    printf("%d\t", seq[j]);
  }
  printf("\nsorted sequnce is:\n");
  bubble_sort (seq, i);
  for (j = 0; j < i; j++)
  {
    printf("%d\t", seq[j]);
  }
  printf("\n");
  return 0;
}
