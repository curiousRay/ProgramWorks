#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NUM 20

#define CTNODE struct ct_node
CTNODE 
{
char name[30];
char school[30];
char city[30];
char eip[10];
char state[30];
CTNODE *next;
};


CTNODE *Enter (CTNODE *h)//------------------------------INPUT----------------------
{
  int curr, num;
  CTNODE *p, *s;
  p = h;

  for (curr = 0; curr < NUM ; curr++)
  {
    s = (CTNODE*)malloc(sizeof(CTNODE));
    printf("请输入姓名：\n");
    scanf("%s", s -> name);
    printf("请输入学校：\n");
    scanf("%s", s -> school);
    printf("请输入城市：\n");
    scanf("%s", s -> city);
    printf("请输入邮编：\n");
    scanf("%s", s -> eip);
    printf("请输入国家：\n");
    scanf("%s", s -> state);

    if (h -> next == NULL)
    {
      h -> next = s;
    }
    else
    {
      p -> next = s;
    }
    p = s;

    printf("继续吗？(y/n):");
    if (getchar() == 'y')
    {
      continue;
    }
    if (getchar() == 'n')
    {
      p -> next = NULL;
      printf("输入成功\n");
      
      //printf("%s", h -> next ->name);
      return h;
    }
  }
} 

CTNODE *Display (CTNODE *h)//------------------------------DISPLAY----------------------
{
  CTNODE *p = h -> next;
  printf("姓名          学校          城市          邮编          国家\n");
  printf("===========================================================================\n");
  if (p == NULL) 
  {
    printf("(无数据)\n");  
  }
  else
  {
    while (p != NULL)
    {
      printf("%-14s%-14s%-14s%-14s%-14s\n",
      p -> name,
      p -> school,
      p -> city,
      p -> eip,
      p -> state);
      p = p -> next; 
    }
  }
  return h;
}

CTNODE *Search (CTNODE *h) //------------------------------SEARCH-------------------
{
  int count = 0;
  char index[30];
  printf("请输入想要查找的姓名：\n");
  scanf("%s", index);
  
  CTNODE *p = h -> next;
  if (p == NULL) printf("查找失败，列表为空\n");
  else
  {
    printf("姓名          学校          城市          邮编          国家\n");
    printf("===========================================================================\n");
    while (p != NULL)
    {
      if (strcmp(index, p -> name) == 0)
      {
        printf("%-14s%-14s%-14s%-14s%-14s\n",
      p -> name,
      p -> school,
        p -> city,
    p -> eip,
      p -> state);
      count ++;  
    }
    p = p -> next;
  }
  if (count == 0) printf("没有找到记录\n");
  else printf("找到%d条记录\n", count); 
  }
  return h;
}

CTNODE *Delete (CTNODE *h)//------------------------------DELETE------------------
{
  int k = 1, pos;
  CTNODE *p = h -> next;
  CTNODE *prev = h;
  if (p == NULL) printf("无法删除，列表为空\n");
  else
  {
    printf("您想删除第几条记录？\n");
    scanf("%d", &pos);
    while (p != NULL)
    {
      if (k == pos) break;
      else
    {
        prev = p;
        p = p -> next;
        k++;
    }
  }
  prev -> next = p -> next;
  free (p);
  printf("删除成功\n");
  }
  return h;
} 

CTNODE *Save (CTNODE *h)//----------------------SAVE---------------------
{
  char filename[30];
  FILE *fp;
  printf("请输入待输出文件的名字：\n");
  scanf("%s", filename);
  fp = fopen(filename, "w");
  if (!fp)
  {
    printf("无法打开文件\n");
    return h;
  }
  else
  {
    CTNODE *p = h -> next;
    if (p == NULL) 
    {
      printf("(无数据)\n");  
    }
    else
    {
      while (p != NULL)
      {
        fprintf(fp, "%-14s%-14s%-14s%-14s%-14s\n",
        p -> name,
        p -> school,
        p -> city,
        p -> eip,
        p -> state);
        p = p -> next; 
      }
      printf("输出成功\n");
      fclose (fp);
    }
    return h; 
  }
}

CTNODE *Load (CTNODE *h)//----------------------LOAD---------------------
{
  char filename[30];
  FILE *fp;
  printf("请输入待装入文件的名字：\n");
  scanf("%s", filename);
  fp = fopen(filename, "r");
  if (!fp)
  {
    printf("无法打开文件\n");
    return h;
  }
  else
  {
    CTNODE *p, *s;
    printf("姓名          学校          城市          邮编          国家\n");
    printf("===========================================================================\n");
    
    while (!feof(fp))
    {
      s = (CTNODE*)malloc(sizeof(CTNODE)); 
      fscanf(fp, "%s\t%s\t%s\t%s\t%s",
      (s -> name),
      (s -> school),
      (s -> city),
      (s -> eip),
      (s -> state));
      
      printf("%-14s%-14s%-14s%-14s%-14s\n", s -> name,
      s -> school,
      s -> city,
      s -> eip,
      s -> state) ;
      if (h -> next == NULL)
      {
        h -> next = s;
      }
      else
      {
        p -> next = s;
      }
      p = s;
    }
    s -> next = NULL;
    fclose (fp);
    printf("装入成功\n");

    return h;
  }
}

int Display_main_menu()
{
  char c;
  do
  {
    system("CLS");
    printf("+-----------------------------------------------------------+\n");
    printf("|              通讯录管理系统      by 雷砺豪(031610118)     |\n");
    printf("+-----------------------------------------------------------+\n");
    printf("1.输入记录\n");
    printf("2.显示所有记录\n");
    printf("3.查询记录\n");
    printf("4.删除记录\n");
    printf("5.输出至文件\n");
    printf("6.从文件装入\n");
    printf("0.退出\n");
    printf("请输入1-6,0: ");
    c = getchar();                                                                                                          
  }
  while (c<'0' || c>'6');
  return (c-'0');
}
int main(void)//--------------------------------MAIN----------------------------
{
  CTNODE *head = (CTNODE*)malloc(sizeof(CTNODE));
  head -> next = NULL;

  for ( ; ; )
  {
    switch (Display_main_menu())
    {
      case 1:
        Enter (head);
        system("PAUSE");
        break;
      case 2:
        Display (head);
        system("PAUSE");
        break;
      case 3:
        Search (head);
        system("PAUSE");
        break;
      case 4:
        Delete (head);
        system("PAUSE");
        break;
      case 5:
        Save(head);
        system("PAUSE");
        break;
    case 6:
        Load (head);
        system("PAUSE");
        break;
      case 0:
        printf("再见！\n");
        system("PAUSE");
        exit(0);
    }
  }
  return 0;
}   