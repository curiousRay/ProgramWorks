#include <stdio.h>
#define ElemType int
#define MAX 100

void select_sort (ElemType seq[], ElemType size)
{
  int i, j, k, tmp;
  for (i = 0; i < size; i++)
  {
    k = i;
    for (j = i + 1; j < size; j++)
    {
      if (seq[j] < seq[k]) k = j;
    }
    if (k != i)
    {
      tmp = seq[k];
      seq[k] = seq[i];
      seq[i] = tmp;
    }
  }
}

int main (void)
{
  ElemType x, seq[MAX];
  int i = 0, j;
  printf("input your origin sequence, type -1 to stop:\n");
  scanf("%d", &x);
  while (x != -1)
  {
    seq[i] = x;
    i++;
    scanf("%d", &x);
  }
  printf("your sequence is:\n");
  for (j = 0; j < i; j++)
  {
    printf("%d\t", seq[j]);
  }
  printf("\nsorted sequnce is:\n");
  select_sort (seq, i);
  for (j = 0; j < i; j++)
  {
    printf("%d\t", seq[j]);
  }
  printf("\n");
  return 0;
}
