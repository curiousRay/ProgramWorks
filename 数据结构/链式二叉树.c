#include <stdio.h>
#include <malloc.h>
#define BiTNode struct bitnode
BiTNode
{
  char data;
  BiTNode *LChild, *RChild;
};

void init (BiTNode *bt)
{
  bt = (BiTNode*)malloc(sizeof(BiTNode));
  bt -> LChild = NULL;
  bt -> RChild = NULL; 
}

BiTNode *create ()
{
  BiTNode *p;
  
  char ch;
  scanf("%c", &ch);
  if(ch != ' ') 
  {
    p = (BiTNode*)malloc(sizeof(BiTNode));
    p -> data = ch;
    p -> LChild = create ();
  p -> RChild = create ();
  } 
  else
  {
    p = NULL;
  }
  return p;
}

void pre_order (BiTNode *bt)
{
  if (bt == NULL) return ;
  printf("%c", bt -> data);
  if (bt -> LChild) pre_order (bt -> LChild);
  if (bt -> RChild) pre_order (bt -> RChild);
}

void in_order (BiTNode *bt)
{
  if (bt == NULL) return ;
  if (bt -> LChild) in_order (bt -> LChild);
  printf("%c", bt -> data);
  if (bt -> RChild) in_order (bt -> RChild);
}

void post_order (BiTNode *bt)
{
  if (bt == NULL) return ;
  if (bt -> LChild) post_order (bt -> LChild);
  if (bt -> RChild) post_order (bt -> RChild);
  printf("%c", bt -> data);
}

int main (void)
{
  printf("请输入二叉树的先序列，结点用字母表示，空域用空格表示。\n");
  BiTNode *bt;
  init (bt);
  bt = create ();

  printf("先序序列：\n");
  pre_order (bt);
  printf("\n");
  
  printf("中序序列：\n");
  in_order (bt);
  printf("\n");
  
  printf("后序序列：\n");
  post_order (bt);
  printf("\n");
  
  return 0;
}