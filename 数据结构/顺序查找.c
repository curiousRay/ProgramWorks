#include <stdio.h>
#define ElemType int
#define MAX 100

int s_search (ElemType seq[], ElemType value, int size)
{
  int j, flag = 0;
  for (j = 0; j < size; j++)
  {
    if (value == seq[j])
    {
      printf("found at position %d\n", j + 1);
      flag = 1;
    }
  }
  if (flag == 0) printf("not found!\n");
  return 0;
}

int main (void)
{
  ElemType x, seq[MAX];
  int i = 0, j, value;
  printf("input your origin sequence, type -1 to stop:\n");
  scanf("%d", &x);
  while (x != -1)
  {
    seq[i] = x;
    i++;
    scanf("%d", &x);
  }
  printf("your sequence is:\n");
  for (j = 0; j < i; j++)
  {
    printf("%d\t", seq[j]);
  }
  printf("\ninput the elem you want to lookup\n");
  scanf("%d", &value);
  s_search (seq, value, i);
  return 0;
}