#include <stdio.h>
#include <string.h>

int found (char elem, char index[], int size)
{
	int i;
	for (i = 0; i < size ; i++)
	{
		if (elem == index[i]) return 1;
	}
	return 0;
}

int main (void)
{
	char s1[100], s2[100], pool[200];
	int i, k = 0, tmp = 0, len1 = 0, len2 = 0, test;
    printf("Please input 2 strings to merge:\n");
    gets(s1);
    while(s1[len1] != '\0') {len1++;}
    gets(s2);
    while(s2[len2] != '\0') {len2++;}  
	strcat(s1, s2);
	for (i = 0; i<len1+len2 ; i++)
	{
 		if (!found(s1[i], pool, k))
		{
			pool[k] = s1[i];
			k++;
		}
	}
	for (test = 0; test <k; test++) printf("%c", pool[test]);

	return 0;
}
