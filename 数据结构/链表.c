#include <stdio.h>
#include <malloc.h>
#define SLNODE struct sl_node
#define ElemType int
SLNODE 
{
ElemType data;
SLNODE *next;
};

SLNODE *create (SLNODE *h)
{
  SLNODE *p, *s;
  ElemType x;
  p = h;
  scanf("%d", &x);
  while (x != -1)
  {
    s = (SLNODE*)malloc(sizeof(SLNODE));
    s -> data = x;
    if (h -> next == NULL)
    {
      h -> next = s;
    }
    else
    {
      p -> next = s;
    }
    p = s;
    scanf("%d", &x);
  }
  p -> next = NULL;
  return h;
}

SLNODE *insert (SLNODE *h, int i, ElemType x)
{
  SLNODE *p, *s;
  int j = 0;
  p = h;
  while (p -> next != NULL && j < i - 1)
  {
    p = p -> next;
    j++;
  }
  if (j != i - 1)
  {
    printf("i is invaild");
    return h;
  }
  else 
  {
    s = (SLNODE*)malloc(sizeof(SLNODE));
    s -> data = x;
    s -> next = p -> next;
    p -> next = s;
    return h;
  }
}

SLNODE *traverse (SLNODE *h)
{
  SLNODE *p = h;
  while (p -> next != NULL)
  {
    printf("%-4d", p -> next -> data);
    p = p -> next;
  }
  return h;
}

SLNODE *delete_node (SLNODE *h, int i) 
{
  SLNODE *p;
  int k = 1;
  while (h -> next != NULL && k < i)
  {
    h = h -> next;
    k++;
  }
  p = h -> next;
  h -> next = p -> next;
  free (p);
  return h;
}

int main (void) 
{
  SLNODE *HEAD;
  int x, i, number; 
  HEAD = (SLNODE*)malloc(sizeof(SLNODE));
  HEAD -> next = NULL;
  printf("create linklist, type -1 to stop:\n");
  create (HEAD);
  traverse(HEAD);
  printf("\ninsert value x before the i-th elem, x i:\n");
  scanf("%d %d", &x, &i);
  insert(HEAD, i, x);
  traverse(HEAD);
  printf("\ninput the x-th elem u want to delete:\n");
  scanf("%d", &number);
  delete_node(HEAD, number);
  traverse (HEAD);
  printf("\n");
  
  return 0;
}
