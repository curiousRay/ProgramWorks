#include <stdio.h>
#define ElemType int
#define MAX 100

void insert_sort (ElemType seq[], ElemType size)
{
  int i, j, key;
  for(i = 1; i < size; i++)
  {
    key = seq[i];
    for(j = i-1; j >=0; --j)
  {
      if(seq[j] > key) seq[j+1] = seq[j];
      else break;
    }
    seq[j+1] = key;
  }
}

int main (void)
{
  ElemType x, seq[MAX];
  int i = 0, j;
  printf("input your origin sequence, type -1 to stop:\n");
  scanf("%d", &x);
  while (x != -1)
  {
    seq[i] = x;
    i++;
    scanf("%d", &x);
  }
  printf("your sequence is:\n");
  for (j = 0; j < i; j++)
  {
    printf("%d\t", seq[j]);
  }
  printf("\nsorted seq is:\n");
  insert_sort (seq, i);
   for (j = 0; j < i; j++)
  {
    printf("%d\t", seq[j]);
  }
   printf("\n");
   return 0;
}
  