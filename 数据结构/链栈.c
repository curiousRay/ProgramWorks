#include <stdio.h>
#include <malloc.h>
#define ElemType int
#define LinkStack struct linkStack
LinkStack
{
  ElemType data;
  LinkStack *next;
};

LinkStack *init ()
{
  LinkStack *top;
  top = (LinkStack*)malloc(sizeof(LinkStack));
  top = NULL;
  return top;
}

LinkStack *push (LinkStack *top, ElemType x)
{
  LinkStack *s;
  s = (LinkStack*)malloc(sizeof(LinkStack));
  s -> data = x;
  s -> next = top;
  top = s;
  return top;
}

LinkStack *pop (LinkStack *top, ElemType x)
{
  LinkStack *p;
  if (top == NULL) return NULL;
  else
  {
    x = top -> data;
    p = top;
    top = top -> next;
    free (p);
    return top;
  }
}

int traverse (LinkStack *top)
{
  LinkStack *p = top;
  
  while (p != NULL)
  {
  printf("%d\t", p -> data);
  p = p -> next;
  }
  printf("\n");
  
  return 0;
}


int main(void)
{
  LinkStack *top;
  ElemType value, popped_elem, x;
  top = init ();
  
  printf("init LinkStack, type -1 to stop:\n");
  scanf ("%d", &x);
  while (x != -1)
  {
    top = push (top, x);
    scanf ("%d", &x);
  }
  printf("the elem you input is:\n");
  traverse (top);
  printf("input value you want to push:\n");
  scanf ("%d", &value);
  top = push (top, value);
  printf("the elem after insert is:\n");
  traverse (top);
  printf ("now pop an elem\n");
  top = pop (top, popped_elem);
  printf("after pop:\n");
  traverse (top);

  return 0;
}