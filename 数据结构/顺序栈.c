#include <stdio.h>
#include <malloc.h>
#define MaxSize 64
#define ElemType int
typedef struct stack
{
  ElemType data[MaxSize];
  int top;
}SqStack;

SqStack *init ()
{
  SqStack *s;
  s = (SqStack*)malloc(sizeof(SqStack));
  s -> top = -1;
  return s;
}

int is_empty (SqStack *s)
{
  if (s -> top == -1) return 1;
  else return 0;
}

int push (SqStack *s, ElemType x)
{
  if (s -> top == MaxSize - 1) return 0;
  else
  {
    s -> top ++;
    s -> data[s -> top] = x;
    return 1;
  }
}

int create (SqStack *s)
{
  ElemType x;
  scanf("%d", &x);
  while (x != -1)
  {
    if (push (s, x) == 0) return 0;
    scanf("%d", &x);
  }
  return 0;
}

int insert (SqStack *s, ElemType value)
{
  int i;
  if (s -> top == MaxSize) return 0;
  s -> data[i + 1] = value;
  s -> top ++;
  return 0;
}

int pop (SqStack *s, ElemType x)
{
  if (is_empty(s)) return 0;
  else
  {
    x = s -> data[s -> top];
    s -> top --;
    return 1;
  }
}

ElemType get_top (SqStack *s)
{
  if (is_empty(s)) return 0;
  else return s -> data[s -> top];
}

int traverse (SqStack *s)
{
  int i;
  if (is_empty(s)) return -1;
  else
  {
    for (i = 0; i <= s -> top; i++)
    {
      printf("%d\t", s -> data[i]);
    }
  printf("\n");
  }
  return 0;
}
SqStack *s;

int main (void)
{
  ElemType value, popped_elem;
  s = init ();
  printf("init SqStack, type -1 to stop:\n");
  create (s);
  printf("the elem you input is:\n");
  traverse (s);
  printf("input value you want to push:\n");
  scanf ("%d", &value);
  push (s, value);
  printf("the elem after insert is:\n");
  traverse (s);
  printf ("now pop an elem\n");
  pop (s, popped_elem);
  printf("after pop:\n");
  traverse (s);
  return 0;
}