#include <stdio.h>
#include <malloc.h>
#define ElemType int
#define QNODE struct QNode

QNODE
{
  ElemType data;
  QNODE *next;
};

typedef struct linkqueue
{
  QNODE *front, *rear;
}LinkQueue;

LinkQueue *init ()
{
  LinkQueue *q;
  QNODE *p;
  q = (LinkQueue*)malloc(sizeof(QNODE));
  p = (QNODE*)malloc(sizeof(QNODE));
  p -> next = NULL;
  q -> front = q -> rear = p;
  return q;
}

void en_queue (LinkQueue *q, ElemType x)
{
  QNODE *p;
  p = (QNODE*)malloc(sizeof(QNODE));
  p -> data = x;
  p -> next = NULL;  
  q -> rear -> next = p;
  q -> rear = p;
}

int is_empty (LinkQueue *q)
{
  if (q -> front == q -> rear) return 1;
  else return 0;
}

int out_queue (LinkQueue *q, ElemType *x)
{
  QNODE *p;
  if (is_empty(q))
  {
    printf("the queue is empty\n");
    return 0;
  }
  else
  {
    p = q -> front -> next;
    q -> front -> next = p -> next;
    *x = p -> data;
    free (p);
    if (q -> front -> next == NULL)
    {
      q -> rear = q -> front;
    }
    return 1;
  }
}

int traverse (LinkQueue *points)
{
  QNODE *p;
  p = points -> front -> next;
  while (1)
  {
      printf ("%d\t", p -> data);
      if (p -> next == NULL)
      {
        break;
    }
    p = p -> next;
  }
  printf ("\n");
  return 0;
}

int main(void)
{
  LinkQueue *points;
  ElemType x, value, outted_elem;
  points = init ();
  printf("init queue, type -1 to stop:\n");
  scanf ("%d", &x);
  while (x != -1)
  {
    en_queue(points, x);
    scanf ("%d", &x);
  }
  printf("the elem you input is:\n");
  traverse (points);
  printf("input value you want to enqueue:\n");
  scanf ("%d", &value);
  en_queue (points, value);
  printf("the elem after insert is:\n");
  traverse (points);
  printf ("now outqueue an elem\n");
  out_queue (points, &outted_elem);
  printf("after pop:\n");
  traverse (points);

  return 0;
}